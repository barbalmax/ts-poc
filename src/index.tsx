import * as ReactHabitat from 'react-habitat'
import PassDataBackAgain from './components/PassDataBackAgain';
import { StandardRows } from "./components/StandardTr";

// ReactDOM.render(<Index />, document.getElementById("root"));

class MyApp extends ReactHabitat.Bootstrapper {
  constructor(){
      super();
    
      // Create a new container builder:
      const builder = new ReactHabitat.ContainerBuilder();

      // Register a component:
      builder.register(StandardRows).as('StandardRows');

      builder.register(PassDataBackAgain).as('PassDataBackAgain');
      builder.registerAsync(()=>  import("./components/Green")).as("Green")
      builder.registerAsync(()=>  import("./components/QuizQuestions")).as("QuizQuestions")

      var container = builder.build()

      this.setContainer(container);
  }
}

// Always export a 'new' instance so it immediately evokes:
export default new MyApp();