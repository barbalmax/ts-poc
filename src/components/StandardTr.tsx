import * as React  from 'react'

type Respository = {
    name : string,
    updatedUnix : number,
    htmlUrl : string
}

interface StandardRowsProps {
    repos: Array<Respository>
}

export function StandardRows({repos} : StandardRowsProps) {
    return(
        repos.map(r=> <StandardTr repo={r} />)
    )
}

interface StandardTrProps {
    repo : Respository
}

export default function StandardTr({ repo } : StandardTrProps ) {

    const words = [
        { key: 'org.repo_updated', value: "last updated " }
    ]

    const i18n = (key : string) => {
        var foundWord = words.find(w => w.key === key)
        if (foundWord) return foundWord.value
        return key
    }

    return (<tr>
        <td data-label="Standard"><a href={repo.htmlUrl}>{repo.name}</a></td>
        <td data-label="OverallLastUpdate">
            <h3>Some text</h3>
            <p className="time">{i18n("org.repo_updated")} {repo.updatedUnix}</p>
        </td>
    </tr>)
}