import * as React from 'react';

interface HabitatProps  {
  /** This is exposed on all props going into habitat */
  proxy : any
}

export default function PassDataBackAgain({proxy} : HabitatProps) {


  const handleChange = (e : React.ChangeEvent<HTMLInputElement>) => {
    proxy.value = e.target.value?.length

    console.log(proxy.value)
  }

  return (
    <input type="text" onChange={handleChange} />
  );
}