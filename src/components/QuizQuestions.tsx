import * as React from 'react';
import DebugJson from './DebugJson';
import Triva from '../services/Triva'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

export default function QuizQuestions() {

  const [questions, setQuestions] = React.useState<Array<any>>([])

  React.useEffect(() => {
    async function getData() {
      var data = await Triva.getQuestions()
      setQuestions(data.results)
    }
    getData()
  }, [])

  return (
    <>
      <h1>Quiz Questions from an API!</h1>
      <div className="App" >
        {
          questions.map( ( q : any) => <Question q={q} />)
        }
      </div>

    </>
  );
}

interface QuestionDto {
  category : string
  question: string,
  incorrect_answers : Array<string>
  correct_answer: string
}

interface QuestionProps {
  q: QuestionDto
}

const Question = ({q}: QuestionProps) => {
  
  const shuffledAnswers = shuffle([...q.incorrect_answers, q.correct_answer])
  return (

    <>
      <div style={{border: '1px solid black', padding: 10}}> 
        <Grid xs={12}>
          Q: {q.question}
        </Grid>
        <Typography variant="h2">Answers</Typography>
        {
          shuffledAnswers.map(a=> <Answer answer={a} isCorrect={a=== q.correct_answer} />)
        }
      </div>
    </>
  )
}

interface AnswerProps {
  answer : string
  isCorrect : boolean
}

const Answer = ({answer, isCorrect} : AnswerProps) => {
  const handleClick = () => {
    if (isCorrect) alert("CORRECT!")

    if (!isCorrect) alert("WRONG")
  }
  
  return (
    <Grid xs={3}><Button onClick={handleClick}>{answer}</Button></Grid>
  )
}


function shuffle(array : Array<any>) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}