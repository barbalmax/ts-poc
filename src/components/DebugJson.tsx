import * as React from 'react';

export default function DebugJson({ json }: any) {

  return (
    <pre>{JSON.stringify(json, null, 2)}</pre>
  );
}


